import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersistence from 'vuex-persist'

const vuexLocal = new VuexPersistence({
  reducer: (state) => ({token: state.token, username: state.username}),
  storage: window.localStorage
})

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    token: "",
    socket: null,
    username: ""
  },
  mutations: {
    set_token(state, token) {
      state.token = token;
    },
    set_username(state, username) {
      state.username = username;
    },
    set_socket(state, socket) {
      state.socket = socket;
    }
  },
  actions: {
    set_token(context, token) {
      context.commit('set_token', token)
      context.dispatch('bind_token')
    },
    bind_token(context) {
      context.state.socket.emit(
        'bind',
        context.state.token
      );
    },
    logout(context) {
      context.dispatch('set_token', "");
      context.commit('set_username', "");
    }
  },
  modules: {
  },
  plugins: [vuexLocal.plugin]
})
